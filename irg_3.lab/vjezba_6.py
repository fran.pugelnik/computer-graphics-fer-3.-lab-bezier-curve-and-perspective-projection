from pyglet.gl import *
import numpy as np
import re
import random
import math
from pyglet.window import mouse
import time

window = pyglet.window.Window(resizable=True)
v = []
f = []

G = [0, 0, 0]
O = [1, 1, 3]

class vrh(object):
    __slots__ = []

vrhovi = [[1, 1, 3], [2, 5, 3], [3, 1, 3]]
t = float(0)


with open('kocka.obj', 'r', encoding = 'utf-8') as file:
    for line in file:
        values = []
        line = re.sub(r'\n', '', line)
        values = line.split(' ')
        if values[0] == 'v':
            v.append([float(values[1]), float(values[2]), float(values[3])])
        elif values[0] == 'f':
            f.append([int(values[1]), int(values[2]), int(values[3])])



@window.event
def on_mouse_press(x, y, button, modifiers):
    if mouse.LEFT:
        global t
        if t <= 1:
            O[0], O[1], O[2] = bezier(vrhovi, t)
            t += 0.01
            on_draw()

@window.event
def on_draw():
    gl.glClearColor(0.0, 0.0, 0.0, 1.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glLoadIdentity()
    glColor3f(random.random(), random.random(), random.random())
    glTranslatef(300, 300, 0.0)
    # glRotatef(45, 0, 0, 0)
    glScalef(100.0, 100.0, 100.0)
    # gluPerspective(90, 1, 0.1, 100)
    glFrontFace(GL_CCW)
    glEnable(GL_CULL_FACE)
    glCullFace(GL_FRONT_AND_BACK)
    glBegin(GL_LINES)
    for element in f:
        prvaTocka = proracunT(v[element[0] - 1])
        glVertex3f(prvaTocka[0], prvaTocka[1], prvaTocka[2])
        drugaTocka = proracunT(v[element[1] - 1])
        glVertex3f(drugaTocka[0], drugaTocka[1], drugaTocka[2])
        trecaTocka = proracunT(v[element[2] - 1])
        glVertex3f(trecaTocka[0], trecaTocka[1], trecaTocka[2])
    # for i in range(0, 100):
    #     x, y, z = bezier(vrhovi, i/100)
    #     glVertex2f(x, y)
    glEnd()




def bezier(vertices, t):

    n = len(vertices)
    x = 0
    y = 0
    z = 0

    for i in range(0, n):

        Bernie = ((math.factorial(n - 1)) / (math.factorial(i) * math.factorial(n - 1 - i))) * math.pow(t, i) * math.pow(1 - t, n - 1 - i)

        # print(vertices[i][0] * Bernie)
        x += vertices[i][0] * Bernie
        y += vertices[i][1] * Bernie
        z += vertices[i][2] * Bernie

    print(x, y, z)
    return x, y, z


def proracunT(vrh):
    G.append(1)
    O.append(1)
    vrh.append(1)

    T_1 = [[1, 0, 0, 0],
          [0, 1, 0, 0],
          [0, 0, 1, 0],
          [-O[0], -O[1], -O[2], 1]]
    G1 = np.dot(G, T_1)

    # G_1 = [G[0] - O[0], G[1] - O[1], G[2] - O[2]]
    # G_2 = [math.sqrt(G_1[0] ** 2 + G_1[1] ** 2), 0, G_1[2]]
    # G_3 = [0, 0, math.sqrt(G_2[0] ** 2 + G_2[2] ** 2)]
    # print(G_1)
    # print(G_2)
    # print(G_3)

    sin_alpha = (G1[1])/(math.sqrt(G1[0]**2 + G1[1]**2))
    cos_alpha = (G1[0])/(math.sqrt(G1[0]**2 + G1[1]**2))
    T_2 = [[cos_alpha, -sin_alpha, 0, 0],
           [sin_alpha, cos_alpha, 0, 0],
           [0, 0, 1, 0],
           [0, 0, 0, 1]]
    G2 = np.dot(G1, T_2)

    sin_beta = (G2[0]) / (math.sqrt(G2[0] ** 2 + G2[2] ** 2))
    cos_beta = (G2[2]) / (math.sqrt(G2[0] ** 2 + G2[2] ** 2))
    T_3 = [[cos_beta, 0, sin_beta, 0],
           [0, 1, 0, 0],
           [-sin_beta, 0, cos_beta, 0],
           [0, 0, 0, 1]]
    G3 = np.dot(G2, T_3)

    T_4 = [[0, -1, 0, 0],
           [1, 0, 0, 0],
           [0, 0, 1, 0],
           [0, 0, 0, 1]]
    T_5 = [[-1, 0, 0, 0],
           [0, 1, 0, 0],
           [0, 0, 1, 0],
           [0, 0, 0, 1]]

    # print(G1)
    # print(G2)
    # print(G3)

    T_1_2 = np.dot(T_1, T_2)
    T_2_3 = np.dot(T_1_2, T_3)
    T_3_4 = np.dot(T_2_3, T_4)
    T_4_5 = np.dot(T_3_4, T_5)


    # Gp = np.dot(G3, T_4_5)
    # print(Gp)

    # H = math.sqrt((O[0] - G[0])**2 + (O[1] - G[1])**2 + (O[2] - G[2])**2)

    P = [[1, 0, 0, 0],
         [0, 1, 0, 0],
         [0, 0, 0, 1/(G3[2])],
         [0, 0, 0, 0]]

    As = np.dot(vrh, T_4_5)
    Ap = np.dot(As, P)

    O.pop()
    G.pop()
    vrh.pop()

    return Ap

pyglet.app.run()
from pyglet.gl import *
import numpy as np
import re
import random
import math
from pyglet.window import mouse

window = pyglet.window.Window(resizable=True)
v = []
f = []

G = [0, 0, 0]
O = [1, 1, 3]


with open('kocka.obj', 'r', encoding = 'utf-8') as file:
    for line in file:
        values = []
        line = re.sub(r'\n', '', line)
        values = line.split(' ')
        if values[0] == 'v':
            v.append([float(values[1]), float(values[2]), float(values[3])])
        elif values[0] == 'f':
            f.append([int(values[1]), int(values[2]), int(values[3])])

# print(v, f)

@window.event
def on_mouse_press(x, y, button, modifiers):
    if mouse.LEFT:
        O[2] += 1
        on_draw()
    elif mouse.RIGHT:
        O[2] -= 1
        on_draw()


@window.event
def on_draw():
    gl.glClearColor(0.0, 0.0, 0.0, 1.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glLoadIdentity()
    glColor3f(random.random(), random.random(), random.random())
    glTranslatef(window.width/2, window.height/2, 0.0)
    # glRotatef(45, 0, 0, 0)
    glScalef(50.0, 50.0, 50.0)
    # gluPerspective(90, 1, 0.1, 100)
    glBegin(GL_LINE_LOOP)
    for element in f:
        prvaTocka = proracunT(v[element[0] - 1])
        glVertex3f(prvaTocka[0], prvaTocka[1], prvaTocka[2])
        drugaTocka = proracunT(v[element[1] - 1])
        glVertex3f(drugaTocka[0], drugaTocka[1], drugaTocka[2])
        trecaTocka = proracunT(v[element[2] - 1])
        glVertex3f(trecaTocka[0], trecaTocka[1], trecaTocka[2])
    glEnd()

@window.event
def on_resize(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(gl.GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0, width, 0, height, -1, 1)
    glMatrixMode(gl.GL_MODELVIEW)


def proracunT(vrh):
    G.append(1)
    O.append(1)
    vrh.append(1)

    T_1 = [[1, 0, 0, 0],
          [0, 1, 0, 0],
          [0, 0, 1, 0],
          [-O[0], -O[1], -O[2], 1]]
    G1 = np.dot(G, T_1)

    # G_1 = [G[0] - O[0], G[1] - O[1], G[2] - O[2]]
    # G_2 = [math.sqrt(G_1[0] ** 2 + G_1[1] ** 2), 0, G_1[2]]
    # G_3 = [0, 0, math.sqrt(G_2[0] ** 2 + G_2[2] ** 2)]
    # print(G_1)
    # print(G_2)
    # print(G_3)

    sin_alpha = (G1[1])/(math.sqrt(G1[0]**2 + G1[1]**2))
    cos_alpha = (G1[0])/(math.sqrt(G1[0]**2 + G1[1]**2))
    T_2 = [[cos_alpha, -sin_alpha, 0, 0],
           [sin_alpha, cos_alpha, 0, 0],
           [0, 0, 1, 0],
           [0, 0, 0, 1]]
    G2 = np.dot(G1, T_2)

    sin_beta = (G2[0]) / (math.sqrt(G2[0] ** 2 + G2[2] ** 2))
    cos_beta = (G2[2]) / (math.sqrt(G2[0] ** 2 + G2[2] ** 2))
    T_3 = [[cos_beta, 0, sin_beta, 0],
           [0, 1, 0, 0],
           [-sin_beta, 0, cos_beta, 0],
           [0, 0, 0, 1]]
    G3 = np.dot(G2, T_3)

    T_4 = [[0, -1, 0, 0],
           [1, 0, 0, 0],
           [0, 0, 1, 0],
           [0, 0, 0, 1]]
    T_5 = [[-1, 0, 0, 0],
           [0, 1, 0, 0],
           [0, 0, 1, 0],
           [0, 0, 0, 1]]

    # print(G1)
    # print(G2)
    # print(G3)

    T_1_2 = np.dot(T_1, T_2)
    T_2_3 = np.dot(T_1_2, T_3)
    T_3_4 = np.dot(T_2_3, T_4)
    T_4_5 = np.dot(T_3_4, T_5)


    # Gp = np.dot(G3, T_4_5)
    # print(Gp)

    # H = math.sqrt((O[0] - G[0])**2 + (O[1] - G[1])**2 + (O[2] - G[2])**2)

    P = [[1, 0, 0, 0],
         [0, 1, 0, 0],
         [0, 0, 0, 1/(G3[2])],
         [0, 0, 0, 0]]

    As = np.dot(vrh, T_4_5)
    Ap = np.dot(As, P)

    O.pop()
    G.pop()
    vrh.pop()

    return Ap




pyglet.app.run()
